z:
{
  # Enable all drivers (even non-free, non-redistributable, etc)
  hardware.enableAllFirmware = true;

  # Use propietary NVIDIA drivers
  nixpkgs.config.allowUnfree = true;
  services.xserver.videoDrivers = [ "nvidia" ];
}

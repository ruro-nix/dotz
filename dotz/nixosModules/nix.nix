z:
let
  inherit (z.inputs) libz;
in {
  nix = {
    # nixPath = (
    #   libz.attr.map.toList
    #   (name: flake: "${name}=${flake.outPath}")
    #   flakes
    # );

    # registry = let
    #   flakeToRegistries = name: flake: {
    #     # "${name}-next" = {
    #     #   exact = false;
    #     #   to = variables.registry.next.${name};
    #     # };
    #     ${name}.to = {
    #       type = "indirect";
    #       id = "${name}-next";
    #       inherit (flake.locked) rev;
    #     };
    #   };
    # in (
    #   libz.attr.concat (
    #     libz.attr.map.toList
    #     flakeToRegistries
    #     flakes
    #   )
    # );

    settings.experimental-features = [
      "nix-command"
      "flakes"
    ];
  };
}

z: { pkgs, ... }:
{
  # List packages installed in system profile.
  environment.systemPackages = with pkgs; with xorg; [
      # Personal (GUI)
      alacritty
      google-chrome

      # Personal (CLI)
      file
      git
      pavucontrol
      ripgrep
      wget

      # X11
      xclip
      xdpyinfo
      xev
      xkill

      # NixOs
      nix-index

      # Hardware
      lm_sensors
      lshw
      pciutils
      sof-firmware
      usbutils
  ];

  # Programs
  programs = {
    dconf.enable = true;
    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
      # ToDo
    };
    zsh = {
      enable = true;
      enableCompletion = true;
      enableBashCompletion = true;
      syntaxHighlighting.enable = true;
      autosuggestions.enable = true;
      histSize = 999999999;
      # ToDo:
      # histFile = "${XDG_DATA_HOME}/zsh_history";
      setOptions = [
        "autocd"               # `cd blah` can be shortened to `blah`
        "numericglobsort"      # expanding * sorts files 8_f 9_f 10_f 11_f 
        "badpattern"           # print an error instead of not doing anything
        "autolist"             # list choiches on multiple valid completions
        "automenu"             # cycle through choices by pressing tab
        "listtypes"            # display type chars after files in the choice list
        "listambiguous"        # complete common prefix before listing choices
        "listpacked"           # display choices in tighter columns
        "autoparamkeys"        # remove space after completion, if its not needed
        "autocontinue"         # disown $PID will also kill -CONT $PID
        "no_banghist"          # don't expand ! in commands
        "histignoredups"       # don't save consecutive duplicates to history
        "histfindnodups"       # ignore all duplicates when searching history
        "histignorespace"      # don't save commands that starts with a ' '
        "histreduceblanks"     # remove extra whitespace from history commands
        "extendedhistory"      # rich timestamps in history
        "no_incappendhistory"  # append commands in history (via sharehistory)
        "sharehistory"         # sync history between multiple shells
      ];
      # ToDo
    };
  };
}

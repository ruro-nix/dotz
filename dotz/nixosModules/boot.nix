z: { pkgs, ... }:
{
  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot = {
      enable = true;
      consoleMode = "auto";
    };
    efi.canTouchEfiVariables = true;
  };

  # Use the latest kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Extra kernel modules.
  boot.kernelModules = [ "coretemp" ];

  # We don't have swap, disable hibernation
  boot.kernelParams = [ "nohibernate" ];
}

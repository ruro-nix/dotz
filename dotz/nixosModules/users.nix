z: { pkgs, ... }:
{
  # ToDo: replace with flakes.home-manager.nixosModules.default
  imports = [ z.inputs.home-manager.nixosModules.home-manager ];

  # Define a user account
  users.mutableUsers = false;
  users.users = let
    common = {
      shell = pkgs.zsh;
      password = "todo";
    };
    normal = {
      # openssh.authorizedKeys.keys = variables.secret.authorized_ssh_keys;
      isNormalUser = true;
      extraGroups = [ "wheel" "sudo" ];
    };
  in {
    root = common;
    ruro = common // normal;
  };

  # Enable sshd
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    # ToDo
  };

  # Use home-manager
  home-manager = {
    useGlobalPkgs = true;
    users.ruro = {};
    sharedModules = z.inputs.dotz.homeModules;
  };
}

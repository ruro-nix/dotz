z:
let
  inherit (z.inputs) libz;
in {
  # Hostname
  # networking.hostName = variables.device.hostname;

  # Configure wpa_supplicant
  networking.wireless = {
    enable = true;
    userControlled.enable = true;
  };

  # Generate the WiFi network list based on
  #     variables.device.wifi_networks
  # and
  #     variables.secret.wifi_passwords
  # networking.wireless.networks = (
  #   libz.list.toAttrs (
  #     libz.list.enumerate (
  #       idx: { name, key ? name }: {
  #         inherit name;
  #         value = {
  #           priority = idx;
  #           psk = variables.secret.wifi_passwords."${key}";
  #         };
  #       }
  #     )
  #     variables.device.wifi_networks
  #   )
  # );

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  # networking.interfaces = (
  #   libz.list.map.toValues
  #   (name: { useDHCP = true; })
  #   variables.device.interfaces
  # );

  # Use loose reverse-path filter. By default, NixOS uses a strict filter, which
  # doesn't accept incoming connections from secondary interfaces.
  # For example, connecting to the same network with Ethernet and WiFi.
  networking.firewall.checkReversePath = "loose";

  # Prevent ARP flux when multiple interfaces connect to the same subnet.
  # boot.kernel.sysctl = (
  #   libz.list.map.toItems
  #   (name: libz.attr.item "net.ipv4.conf.${name}.arp_ignore" 1)
  #   variables.device.interfaces
  # );
}

z: final: prev:
{
  nixosSystem = args:
    let
      inherit (z.inputs.dotz) nixosModules;
      modules = (args.modules or []) ++ nixosModules;
      nextArgs = args // { inherit modules; };
    in
      prev.nixosSystem nextArgs;
}

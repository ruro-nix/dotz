z: { pkgs, ... }:
{
  gtk = let
    extraConfig = {
      gtk-application-prefer-dark-theme = true;
      gtk-button-images = true;
      gtk-menu-images = true;
      gtk-primary-button-warps-slider = true;
      gtk-decoration-layout = ":minimize,maximize,close";
      gtk-enable-animations = true;
      gtk-toolbar-style = 2;
      #gtk-fallback-icon-theme=breeze-dark
      #gtk-cursor-theme-name=breeze_cursors
      #gtk-cursor-theme-size=24
      #gtk-font-name=Noto Sans,  10
    };
  in {
    enable = true;
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
    };
    theme = {
      package = pkgs.arc-theme;
      name = "Arc-Dark";
    };
    font = {
      package = pkgs.noto-fonts;
      name = "Noto Sans";
      size = 10;
    };
    gtk3.extraConfig = extraConfig;
    #gtk2.extraConfig = extraConfig;
    #gtk4.extraConfig = extraConfig;
  };
}

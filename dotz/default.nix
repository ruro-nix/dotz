z:
let
  inherit (z.inputs) libz devz;

  # Auto populate exports from folders
  exports.auto = [./.];

  # Load device-specific static configuration from the device path
  static = import (devz.outPath + "/static.nix");
  inherit (static) system hostname;

  # Gather all the exported NixOS module overlays
  nixosModules = libz.flakes.getOverlays "nixosModules" z.inputs;

  # Gather all the exported home-manager module overlays
  homeModules = libz.flakes.getOverlays "homeModules" z.inputs;

  # Build the system configuration
  nixosConfigurations.${hostname} = libz.nixosSystem {inherit system;};
in
  {inherit system hostname nixosModules homeModules nixosConfigurations exports;}

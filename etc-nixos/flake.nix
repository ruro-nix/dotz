{
  inputs = {
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # External Flakes
    nixpkgs.url = "nixpkgs-next";
    home-manager.url = "home-manager-next";

    # Personal Flakes
    devz.url = "devz-next";
    dotz.url = "dotz-next";
    libz.url = "libz-next";
  };

  outputs = devInputs: (
    devInputs.devz.makeDevConfig {
      inherit devInputs;
      devLock = ./flake.lock;
    }
  );
}

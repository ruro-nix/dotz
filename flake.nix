{
  description = "Common system configuration for my nix systems";
  outputs = { z, ... }: import ./dotz z;
}
